package threadingpractice.chelsee;

import java.util.Random;

public class MessageDisplay implements Runnable{

    private String message;
    private int sleep;
    private String threadName;

    static void threadName(String name){

        String threadName = Thread.currentThread().getName();
        System.out.format("%s: %s%n", threadName);
    }

    public MessageDisplay(String message, int sleep) {

        this.message = message;
        this.sleep = sleep;
    }

    private void threadMessage(int i) {

        System.out.println(message + " waiting...");
    }

    public void run() {

        System.out.println("\nExecuting thread named: " + threadName + ", Sleep set to: " + sleep + "\n");

        for (int i = 0; i < message.length(); i++) {
            try {
                Thread.sleep(sleep);
                threadMessage(i);
            } catch (InterruptedException e) {
                System.err.println(e.toString());
            }
        }
        System.out.println("\n" + threadName + ": " + message + " Finished!\n");
    }

}
