package threadingpractice.chelsee;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainThread {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(3);

        MessageDisplay message1 = new MessageDisplay("Idaho is famous for potatoes.", 20);
        MessageDisplay message2 = new MessageDisplay("Utah is famous for skiing.", 1);
        MessageDisplay message3 = new MessageDisplay("Arizona is famous for the Grand Canyon.", 5);
        MessageDisplay message4 = new MessageDisplay("Texas is famous for the heat!", 100);
        MessageDisplay message5 = new MessageDisplay("New York is famous for Broadway.", 3);
        MessageDisplay message6 = new MessageDisplay("Florida is famous for beaches.", 15);

        myService.execute(message1);
        myService.execute(message2);
        myService.execute(message3);
        myService.execute(message4);
        myService.execute(message5);
        myService.execute(message6);

        myService.shutdown();
    }
}
