package ExceptionsAndValidation;

public class DivisionMethodClass {
    //Write the division method
    static void doDivision(int num1, int num2) {

        //Return the result
        //Include a throws clause - 0 as num2
        if (num2 == 0) {
            throw new ArithmeticException("Cannot divide by zero!");
        } else {
            int divideTotal = (num1 / num2);
            System.out.println();
            System.out.println("The division of number 1 and number 2 is: " + divideTotal);
        }
    }
}
