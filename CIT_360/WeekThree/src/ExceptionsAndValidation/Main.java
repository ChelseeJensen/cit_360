package ExceptionsAndValidation;

import java.util.Scanner;
import static ExceptionsAndValidation.DivisionMethodClass.doDivision;

public class Main {

    public static void main(String[] args){

        //Gather the input of two numbers from the keyboard
        //Use exception handling to catch the most specific exception possible if the user enters 0 for num2
        //Display a descriptive message in the catch to tell the user the problem
        //Give the user a second chance to enter the data
        //Call doDivision Method
        //Display a message in the final statement
        Scanner input = new Scanner(System.in);
        int num1 = 0;
        int num2 = 0;

        try{
            System.out.println("Please enter a whole, real number: ");
            num1 = input.nextInt();

            System.out.println("Please enter a second whole, real number: ");
            num2 = input.nextInt();
            doDivision(num1, num2);
        }
        catch (ArithmeticException e) {
            System.out.println("Cannot divide by zero! Please re-enter a second whole, real number:");
            num2 = input.nextInt();
            doDivision(num1, num2);
        }
        finally {
            System.out.println("Thanks for trying this division program!");
        }
    }
}
