package jensenc.exceptions.validation;

import java.util.Scanner;
import static jensenc.exceptions.validation.ValidateMain.doDivision;



public class MainVersionTwo {

    public static void main(String[] args){
        MainVersionTwo validateinput = new MainVersionTwo();
        validateinput.validateScanner();
    }

    private void validateScanner(){
        Scanner input = new Scanner(System.in);
        int num1 = 0;
        int num2 = 0;

        do{
           System.out.println("Please enter a whole, real number: ");
           while (!input.hasNextInt()){
               System.out.println("That is not a valid number. Please enter a whole, real number: ");
               input.next();
           }
           num1 = input.nextInt();
        }
        while (0 >= num1 || num1 < 0);

        do{
            System.out.println("Please enter a second whole, real number: ");
            while (!input.hasNextInt()){
                System.out.println("That is not a valid number. Please enter a whole, real number: ");
                input.next();
            }
            num2 = input.nextInt();
        }
        while (num2 == 0);

        doDivision(num1, num2);

        System.out.println("Thanks for trying this division program!");
    }
}
