package jensenc.edu;

public class Meals {

    private String mainCourse;
    private String sideDish;

    public Meals(String mainCourse, String sideDish){
        this.mainCourse = mainCourse;
        this.sideDish = sideDish;
    }

    public String toString() {
        return "Main Course: " + mainCourse + " | Side Dish: " + sideDish;
    }
}
