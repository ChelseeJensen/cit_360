package com.example.final_project;

public class Objects {

    //s data member will hold an int value chosen by the user
    private int s = 0;

    //This method acts as a constructor
    public int Amount() {
    }

    //This method acts as a constructor with specific parameters
    public int Amount(int s) {
        this.s = s;
    }

    //This is a getter method which will safely retrieve the s data member
    public int getS() {return s;}

    //This is a setter method which will safely set the s data member
    public void setS(int s) {this.s = s;}
}
