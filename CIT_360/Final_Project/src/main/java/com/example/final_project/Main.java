package com.example.final_project;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int s = 0;

        System.out.println("This program will give you meal ideas, the amount depending on the number you enter here: ");
        try {
            s = input.nextInt();
        }
        catch (NumberFormatException e) {
            System.out.println("Not a real number! Please enter a whole, real number: ");
            s = input.nextInt();
        }
        finally {
            System.out.println("Thanks for using this program!");
        }

        System.out.println("----- Meal Ideas Set -----");
        Set ideaSet = new TreeSet();
        ideaSet.add("Lasagna");
        ideaSet.add("Spaghetti");
        ideaSet.add("Alfredo Fettuccine");
        ideaSet.add("Roast");
        ideaSet.add("Hamburgers");
        ideaSet.add("Hot Dogs");
        ideaSet.add("Sloppy Jo");
        ideaSet.add("BBQ Chicken");
        ideaSet.add("Taco Salad");
        ideaSet.add("Hawaiian Haystacks");
        ideaSet.add("Chicken Enchiladas");
        ideaSet.add("Chicken Pot Pie");
        ideaSet.add("Pea Soup");
        ideaSet.add("Taco Soup");
        ideaSet.add("Taco Salad");

        for (Object str : ideaSet) {
            System.out.println((String)str);
        }


        ExecutorService myService = Executors.newFixedThreadPool(5);

        HelloThreads mealIdea1 = new HelloThreads("Corn", 500);

        myService.execute(mealIdea1);

        myService.shutdown();
    }
}
