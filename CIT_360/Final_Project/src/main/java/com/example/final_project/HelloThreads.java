package com.example.final_project;

public class HelloThreads implements Runnable {

    private int sleep;
    private String mealIdea;

    public HelloThreads(String mealIdea, int sleep) {
        this.mealIdea = mealIdea;
        this.sleep = sleep;
    }

    private void threadMessage(int i){
        System.out.println(mealIdea + " waiting... ");
    }

    public void run() {
        for (int i = 0; i < mealIdea.length(); i++) {
            try{
                Thread.sleep(sleep);
                threadMessage(i);
            }
            catch (InterruptedException e) {
                System.err.println(e.toString());
            }
        }
        System.out.println("/n" + mealIdea + );
    }
}
