package com.example.final_project;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class HelloServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //Display Number of Meal Ideas once form (number) has been submitted
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        String s = request.getParameter("mealIdeas");
        s = int.Main.parseInt(s);


        out.println("<h1>Here are " + s + "meal ideas: </h1>");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Hello
        PrintWriter out = response.getWriter();
        out.println("This Information is Not Readily Available");
    }

    public void destroy() {
    }
}