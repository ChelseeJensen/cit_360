package jsonhttpurl.chelsee;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class Main {
    //First Class: HTTP server component that takes an object and converts it to JSON
    //Then returns a block of JSON using HTTP
    //Include error handling, and data validation (catching for most specific exceptions)

    public static void main(String[] args) {

        Temple temp = new Temple();
        temp.setName("Idaho Falls Temple");
        temp.setLocation("Idaho Falls, Idaho, USA");
        temp.setOperating(true);

        String json = MainServer.templeToJSON(temp);
        System.out.println(json);

        Temple temple2 = MainClient.JSONtoTemple(json);
        System.out.println(temple2);
    }


}
