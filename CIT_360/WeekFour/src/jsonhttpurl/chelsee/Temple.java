package jsonhttpurl.chelsee;

public class Temple {

    private String name;
    private String location;
    private int phone;
    private boolean operating;

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getLocation(){
        return location;
    }

    public void setLocation(String location){
        this.location = location;
    }

    public boolean getOperating(){
        return operating;
    }

    public void setOperating(boolean operating) {
        this.operating = operating;
    }

    public String toString(){
        return "Name: " + name + " Location: " + location + " Operating: " + operating;
    }
}
