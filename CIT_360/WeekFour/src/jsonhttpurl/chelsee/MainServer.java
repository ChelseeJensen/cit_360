package jsonhttpurl.chelsee;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.io.*;

public class MainServer {


    public static String templeToJSON(Temple temple){

        ObjectMapper mapper = new ObjectMapper();
        String t = "";

        try{
            t = mapper.writeValueAsString(temple);
        }
        catch(JsonProcessingException e){
            System.err.println(e.toString());
        }

        return t;
    }

    public static String MainClient(String string){
        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection)  url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringbuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null){
                stringbuilder.append(line + "\n");
            }

            return stringbuilder.toString();
        }

        catch (IOException e) {
            System.err.println(e.toString());
        }
        return "Error";
    }


/*    public static void main(String[] args) {
        System.out.println(MainServer.MainClient("http://www.google.com"));
    }
*/
}
