package jsonhttpurl.chelsee;

//Second Class: Client component that makes a call to the server
//Gets the JSON it returns and then converts it into an object
//Include error handling, and data validation (catching for most specific exceptions)

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MainClient {

    public static Temple JSONtoTemple(String t) {
        ObjectMapper mapper = new ObjectMapper();
        Temple temple = null;

        try {
            temple = mapper.readValue(t, Temple.class);
        }
        catch (JsonProcessingException e){
            System.err.println(e.toString());
        }

        return temple;
    }
}
