package edu.jensenc;

import java.util.*;

/* This program is intended for meal planning purposes.
 * This first week I decided to keep it rather simple to showcase the required Java Collections Types.
 * Ultimately I would like to expand on this program.
 * It should:
 * -Store a list of meal ideas
 * -Allow users to add their own meal ideas (and save those meals so when the program is reopened they are still available and don't have to be re-entered)
 * -Randomly generate meal options
 * -Link a side dish/veggie to each meal
 * -Allow users to input the month, and the program generates the correct number of meals for that month (including leap years)
 * I plan to keep playing with it because this is a program I would use every month if I can get it to work! */

public class WeekTwo {

    public static void main(String[] args){

        //Display a list of meal ideas for meal planning

        System.out.println("----- Meal Ideas List -----");
        List ideaList = new ArrayList();
        ideaList.add("Lasagna");
        ideaList.add("Spaghetti");
        ideaList.add("Alfredo Fettuccine");
        ideaList.add("Roast");
        ideaList.add("Hamburgers");
        ideaList.add("Hot Dogs");
        ideaList.add("Sloppy Jo");
        ideaList.add("BBQ Chicken");
        ideaList.add("Taco Salad");
        ideaList.add("Hawaiian Haystacks");
        ideaList.add("Chicken Enchiladas");
        ideaList.add("Chicken Pot Pie");
        ideaList.add("Pea Soup");
        ideaList.add("Taco Soup");
        ideaList.add("Taco Salad");

        for (Object str : ideaList) {
            System.out.println((String)str);
        }

        //Display a list of meal ideas for meal planning, excluding any duplicates

        System.out.println("----- Meal Ideas Set -----");
        Set ideaSet = new TreeSet();
        ideaSet.add("Lasagna");
        ideaSet.add("Spaghetti");
        ideaSet.add("Alfredo Fettuccine");
        ideaSet.add("Roast");
        ideaSet.add("Hamburgers");
        ideaSet.add("Hot Dogs");
        ideaSet.add("Sloppy Jo");
        ideaSet.add("BBQ Chicken");
        ideaSet.add("Taco Salad");
        ideaSet.add("Hawaiian Haystacks");
        ideaSet.add("Chicken Enchiladas");
        ideaSet.add("Chicken Pot Pie");
        ideaSet.add("Pea Soup");
        ideaSet.add("Taco Soup");
        ideaSet.add("Taco Salad");

        for (Object str : ideaSet) {
            System.out.println((String)str);
        }

        //Display a list of meal ideas in alphabetical order for meal planning

        System.out.println("----- Meal Ideas Queue -----");
        Queue ideaQueue = new PriorityQueue();
        ideaQueue.add("Lasagna");
        ideaQueue.add("Spaghetti");
        ideaQueue.add("Alfredo Fettuccine");
        ideaQueue.add("Roast");
        ideaQueue.add("Hamburgers");
        ideaQueue.add("Hot Dogs");
        ideaQueue.add("Sloppy Jo");
        ideaQueue.add("BBQ Chicken");
        ideaQueue.add("Taco Salad");
        ideaQueue.add("Hawaiian Haystacks");
        ideaQueue.add("Chicken Enchiladas");
        ideaQueue.add("Chicken Pot Pie");
        ideaQueue.add("Pea Soup");
        ideaQueue.add("Taco Soup");
        ideaQueue.add("Taco Salad");

        Iterator iterator = ideaQueue.iterator();
        while (iterator.hasNext()) {
            System.out.println(ideaQueue.poll());
        }

        //Display a specific number of Meal Ideas for meal planning
        //Replace duplicate keys (in this case, numbers)
        System.out.println("----- Meal Ideas Map -----");
        Map ideaMap = new HashMap();
        ideaMap.put(1, "Lasagna");
        ideaMap.put(2, "Spaghetti");
        ideaMap.put(3, "Alfredo Fettuccine");
        ideaMap.put(4, "Roast");
        ideaMap.put(5, "Hamburgers");
        ideaMap.put(6, "Hot Dogs");
        ideaMap.put(7, "Sloppy Jo");
        ideaMap.put(8, "BBQ Chicken");
        ideaMap.put(9, "Taco Salad");
        ideaMap.put(10, "Hawaiian Haystacks");
        ideaMap.put(11, "Chicken Enchiladas");
        ideaMap.put(12, "Chicken Pot Pie");
        ideaMap.put(13, "Pea Soup");
        ideaMap.put(14, "Taco Soup");
        ideaMap.put(15, "Taco Salad");
        ideaMap.put(15, "Alfredo Fettuccine");

        for (int i =1; i < 16; i++) {
            String result = (String)ideaMap.get(i);
            System.out.println(result);
        }
    }
}
